# README #

## Desktop Installation Instructions ##
1. Make sure this page is open in Google Chrome
2. Install Tampermonkey for Chrome [LINK](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en)
3. Install [Userscript](https://bitbucket.org/evan_petersen/intellitime/raw/master/Intellitime.user.js).  Note: it will look like nothing happened after you click install.

## Desktop Usage ##
After following the installation instructions, open [Intellitime](https://cosp.intellitime.net/VTI/DefaultFrameSet.aspx?Direct=1) using Google Chrome and you will see an enhanced view. If at any time you wish to disable the new view, click the tampermonkey icon in the top right and uncheck Enabled (the first option in the list).


## Android Installation Instructions ##
1. Install [Kiwi Browser](https://play.google.com/store/apps/details?id=com.kiwibrowser.browser&hl=en_US&gl=US) for Android
2. Using Kiwi Browser, install [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en).
3. Install [Userscript](https://bitbucket.org/evan_petersen/intellitime/raw/master/Intellitime.user.js).  Note: it will look like nothing happened after you click install.

## Android Usage ##
After following the installation instructions, open [Intellitime](https://cosp.intellitime.net/VTI/DefaultFrameSet.aspx?Direct=1) using Kiwi Browser.  You can create a shortcut on your home screen by clicking the vertical three dots and selecting "Add to Home Screen."

This software comes with no warranty and no guarantees.
Should you have issues with this software, please disable the script in Tampermonkey.