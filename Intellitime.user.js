// ==UserScript==
// @name         Intellitime
// @namespace    http://www.evanpetersen.com
// @version      0.3.24
// @description  Resolve Intellitime Styling
// @author       Evan Petersen
// @updateURL    https://bitbucket.org/evan_petersen/intellitime/raw/master/Intellitime.user.js
// @downloadURL  https://bitbucket.org/evan_petersen/intellitime/raw/master/Intellitime.user.js
// @match        https://cosp.intellitime.net/*
// @require      https://code.jquery.com/jquery-3.5.1.min.js
// @grant        none
// ==/UserScript==

var ShiftDate = function(date = new Date()) {

    //Reset current date to beginning of day
    date.setHours(0,0,0,0);

    // Number of milliseconds between
    let oneDay = 1000 * 60 * 60 * 24;
    let shiftArray = ["C","B","C","B","A","B","A","C","A"];
    let beginningOfTime = new Date(2019, 1, 4);

    let timeBetween = date.getTime() - beginningOfTime.getTime();

    // Number of Days between
    let daysBetween = Math.round(
        timeBetween / oneDay
    );

    // Set our spot in the sequence
    let arrayPosition = daysBetween % 9;
    let shiftLetter = shiftArray[arrayPosition];

    return shiftLetter;
}

var locations;

function addGlobalStyle(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}

(function ($, undefined) { $(function () {

    'use strict';

    console.log(window.location.pathname);
    
    // https://trello.com/c/Myu4BW6R/12-make-shift-status-font-black-and-font-weight-bold

    $('span.shiftlabel').css({'color': 'black', 'font-weight': 'bold'});

    // https://trello.com/c/FlgJDMS6/3-update-small-calendar-in-roster-to-have-shift-day-colors

    function elementLoaded(el, cb, count = 0) {
        if ($(el).length) {
            // Element is now loaded.
            console.log('found calendar!');
            cb($(el));
        } else {

            if (count > 100) {
                //console.log('did not find calendar');
                return;
            }
            // Repeat every 500ms.
            setTimeout(function() {
                elementLoaded(el, cb, ++count)
            }, 500);
        }
    };

    elementLoaded('div#PU_PopupDivcalPOP', function(el) {

        //console.log('document is ready');
        let calendarPopUp = el;
        //console.log(calendarPopUp);
        calendarPopUp.bind('DOMSubtreeModified', function(){
            //console.log('calheader updated')
            let calDates = $('td.CalDay').not('.CalDayPPCell');
            let monthYear = $('td.CalHeader').first().text();

            calDates.each(function(index) {
                if ($(this).text() > 0) {
                    //console.log('parsing date for ' + $(this).text());
                    //console.log('month year= ' + monthYear);
                    let tempDate = new Date($(this).text() + ' ' + monthYear);
                    let calShift = ShiftDate(tempDate);
                    $(this).addClass("shift-class-" + calShift);

                    //console.log(tempDate);
                }
            });
        });

    });

    // https://trello.com/c/BF9zBTkz/1-update-ppo-to-be-styled-like-cp-and-lt

    addGlobalStyle(`
        .caldatacell span.calsickflag { color: white !important; }
        .caldatacell a { color: white; }
        .caldatacell a:hover { color: #ddd }

        .shift-class-A { background-color: red; color: white }
        .shift-class-B { background-color: blue; color: white }
        .shift-class-C { background-color: green; color: white }

        .shift-class-A a.Std_A, .shift-class-B a.Std_A, .shift-class-C a.Std_A {
            color: white;
        }

        .shift-class-A a.Std_A:hover, .shift-class-B a.Std_A:hover, .shift-class-C a.Std_A:hover {
            color: #ddd;
        }
    `);

    // https://trello.com/c/rwSvLt44/20-after-sorting-non-working-people-to-the-bottom-of-a-station-ambiguous-entries-appear-because-intellitime-relies-on-subsequent-po

    if (typeof window.SchedObs != "undefined") {
        //console.log(window.SchedObs);

        $("td[id^='ctl00_PageData_sgd_RosterEmpName_']:empty").each( function(index) {

            let empId = $(this).attr('id');
            let schedObsId = empId.replace(new RegExp(/^.*_/), '');
            let userId = window.SchedObs[schedObsId - 1].UserID;

            let empName = '';
            try {
                empName = window.arrUserInformations[userId].EmpName;
            } catch (e) {
                console.log(e);
                empName = 'Employee missing shift schedule';
            }
            $(this).append('<a class="entry-a" onclick="setSGWithinMyselfUpload();" href="javascript:contextMenuPopupEmpName(\'RosterEmpName_' + schedObsId + '\', ' + userId + ');">' + empName + '</a>');

        });

    }

    // https://trello.com/c/uot1vmWy/14-no-background-color-in-the-name-box-red-text-for-officers-and-blue-text-for-medics

    $( "td[id^='ctl00_PageData_sgd_RosterJobName']" ).filter(":contains('CP'), :contains('LT')").each(function( index ) {

        $(this).css({"background-color": "red", "color": "white"})
            .siblings()
            .first()
            .css("color","red")
            .children()
            .css({"color": "red", "text-transform": "uppercase", "font-weight": "bold"});
    });


    // https://trello.com/c/OjZpTCjO/18-earmark-ppo-with-blue-stripe

    $( "td[id^='ctl00_PageData_sgd_RosterJobName']" ).filter(":contains('PPO')").each(function( index ) {
        $(this).css({"background": "linear-gradient(90deg, red 85%, blue 15%)", "color": "white"})
            .siblings()
            .first()
            .css("color", "red")
            .children()
            .css({"color": "red", "text-transform": "uppercase", "font-weight": "bold"});
    });

    $( "td[id^='ctl00_PageData_sgd_RosterJobName']" ).filter(":contains('DR')").each(function( index ) {
        $(this).css({"background-color": "black", "color": "white"})
            .siblings()
            .first()
            .css("color","black")
            .children()
            .css({"color": "black", "text-transform": "uppercase", "font-weight": "bold"});
    });

    $( "td[id^='ctl00_PageData_sgd_RosterJobName']" ).filter(":contains('PM')").each(function( index ) {
        $(this).css({"background-color": "blue", "color": "white"})
            .siblings()
            .first()
            .css("color","blue")
            .children()
            .css({"color":"blue", "text-transform": "uppercase", "font-weight": "bold"});
    });

    $( "td[id^='ctl00_PageData_sgd_RosterJobName']" ).filter(":contains('FF')").each(function( index ) {
        $(this).css({"background-color": "yellow"})
            .siblings()
            .first()
            .css("color","#ffa96c")
            .children()
            .css({"color": "#ffa96c","text-transform": "uppercase", "font-weight": "bold"});
    });


    // Color code Hazmat full entry - Updated 5/9/21
    $( "td[id^='ctl00_PageData_sgd_RosterEmpID']" ).filter(":contains('102196'), " +
":contains('106085'), " +
":contains('107108'), " +
":contains('108824'), " +
":contains('110187'), " +
":contains('110301'), " +
":contains('110303'), " +
":contains('110701'), " +
":contains('110703'), " +
":contains('111765'), " +
":contains('111768'), " +
":contains('111769'), " +
":contains('111774'), " +
":contains('111775'), " +
":contains('112678'), " +
":contains('112679'), " +
":contains('112690'), " +
":contains('113224'), " +
":contains('113741'), " +
":contains('115166'), " +
":contains('115173'), " +
":contains('115709'), " +
":contains('115724'), " +
":contains('116015'), " +
":contains('116027'), " +
":contains('116703'), " +
":contains('117066'), " +
":contains('117069'), " +
":contains('117415'), " +
":contains('117429'), " +
":contains('117751'), " +
":contains('117760'), " +
":contains('117761'), " +
":contains('117762'), " +
":contains('117998'), " +
":contains('118000'), " +
":contains('118005'), " +
":contains('118007')").each(function( index ) {
            $(this)
                .css({"position": "relative", "color": "white"})
                .prepend('<img src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNkdmb+DwAB5AFKzN4OSQAAAABJRU5ErkJggg==" style="width:100%; height: 100%;"/>')
                .children().css({"position": "absolute", "top": "0px", "left": "0px", "color": "white"})
                //.css({"background": "url(data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNkYPj/HwADBwIAMCbHYQAAAABJRU5ErkJggg==)", "color": "white"});
        });

    // Color code Hazmat halfway Updated 5/9/21
    $( "td[id^='ctl00_PageData_sgd_RosterEmpID']" ).filter(":contains('104659'), " +
":contains('107516'), " +
":contains('108174'), " +
":contains('108826'), " +
":contains('109374'), " +
":contains('109377'), " +
":contains('110048'), " +
":contains('110186'), " +
":contains('110281'), " +
":contains('111730'), " +
":contains('112180'), " +
":contains('112186'), " +
":contains('112192'), " +
":contains('112199'), " +
":contains('112201'), " +
":contains('112523'), " +
":contains('112530'), " +
":contains('112665'), " +
":contains('112668'), " +
":contains('113220'), " +
":contains('113233'), " +
":contains('113743'), " +
":contains('113746'), " +
":contains('114116'), " +
":contains('115182'), " +
":contains('115719'), " +
":contains('116302'), " +
":contains('116666'), " +
":contains('116924')").each(function( index ) {
            $(this)
                .css({"position": "relative", "color": "white"})
                .prepend('<img src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNkdmb+DwAB5AFKzN4OSQAAAABJRU5ErkJggg==" style="width:50%; height: 100%;"/>')
                .children().css({"position": "absolute", "top": "0px", "left": "0px", "color": "white"})
                //.css({"background": "url(data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNkYPj/HwADBwIAMCbHYQAAAABJRU5ErkJggg==)", "color": "white"});
        });

    // https://trello.com/c/369YQHxq/24-re-color-4th-class
    // Pink 4th class
    $( "td[id^='ctl00_PageData_sgd_RosterAllUserSkills']" ).filter(":contains('4C')").each(function( index ) {
            $(this).siblings()
                .first()
                .css("color","#ff00ff")
                .children()
                .css({"color": "#ff00ff","text-transform": "uppercase", "font-weight": "bold"});
        });

    // https://trello.com/c/a8W69PVQ/19-add-option-to-click-next-shift-previous-shift

    let previousButton = $("input#ctl00_PageData_sgd_SGPrevDisplayButton");
    if (previousButton.length == 1) {
        let previousShiftButton = previousButton.clone(false);
        let nextShiftButton = previousButton.clone(false);

        let spacer = previousButton.prev().clone();
        let spacer2 = spacer.clone();

        previousShiftButton.val('Previous Shift');
        previousShiftButton.css('width','98px');
        previousShiftButton.prop("onclick", null);
        previousShiftButton.click(function() {
            let curDate = new Date($('input#ctl00_PageData_sgd_DisplayStartDate').val());
            let curShift = ShiftDate(curDate);

            let prevDate = curDate;
            let prevShift = null;

            while (prevShift != curShift) {
                prevDate.setDate(prevDate.getDate() - 1);
                prevShift = ShiftDate(prevDate);
            }

            $("input#ctl00_PageData_sgd_DisplayStartDate").val((prevDate.getMonth() + 1) + '/' + prevDate.getDate() + '/' + prevDate.getFullYear());
            $("input#ctl00_PageData_sgd_SGRefreshButton_1").trigger("click");

        });
        previousShiftButton.insertBefore(previousButton);
        spacer.insertBefore(previousButton);

        nextShiftButton.val('Next Shift');
        nextShiftButton.css('width','76px');
        nextShiftButton.prop("onclick", null);
        nextShiftButton.click(function() {
            let curDate = new Date($('input#ctl00_PageData_sgd_DisplayStartDate').val());
            let curShift = ShiftDate(curDate);

            let nextDate = curDate;
            let nextShift = null;

            while (nextShift != curShift) {
                nextDate.setDate(nextDate.getDate() + 1);
                nextShift = ShiftDate(nextDate);
            }

            $("input#ctl00_PageData_sgd_DisplayStartDate").val((nextDate.getMonth() + 1) + '/' + nextDate.getDate() + '/' + nextDate.getFullYear());
            $("input#ctl00_PageData_sgd_SGRefreshButton_1").trigger("click");
        });

        let nextButton = $("input#ctl00_PageData_sgd_SGNextDisplayButton");
        nextShiftButton.insertAfter(nextButton);
        spacer2.insertAfter(nextButton);
    }

    // https://trello.com/c/hz2FP3ex/2-move-non-working-personnel-on-schedule-to-bottom-of-station-list

    var nonScheduled = $("span.shiftlabel:not(" +
        ":contains('Scheduled')," +
        ":contains('Shipout')," +
        ":contains('Hireback')," +
        ":contains('Acting as Driver')," +
        ":contains('Acting as Lieutenant')," +
        ":contains('Acting as Battalion Chief')," +
        ":contains('Acting as Driver/Officer')," +
        ":contains('Swap In')," +
        ":contains('Pro-Pay Off')," +
        ":contains('Mandatory')," +
        ":contains('Acting as Paramedic')," +
        ":contains('Grant Hazmat ODP-Staffing Shortage')," +
        ":contains('.SMART Voluntary')," +
        ":contains('.SMART Swap')," +
        ":contains('.SMART - Mandatory')," +
        ":contains('Act as Dr/Swap in')," +
        ":contains('Meeting')" +
    ")")

    nonScheduled.each( function(index) {
        let nonScheduleRow =
            $(this).closest("tr")
                .parent()
                .closest("tr");

        // https://trello.com/c/DpEO39HJ/17-grey-out-rank-if-somebody-is-not-actually-working
        $(nonScheduleRow).find("td[id^='ctl00_PageData_sgd_RosterJobName']")
            .css({"background-color": "grey", "color": "white"})
            .siblings()
            .first()
            .css('color', 'grey')
            .children()
            .css('color', 'grey');

        let found = false;
        let nextRow = nonScheduleRow;

        while(!found) {
            //console.log('Evaluating next row');
            nextRow = nextRow.next();
            //console.log(nextRow);

            if (nextRow.length == 0) {
                found = true;
            }

            let result = nextRow.find("td.empgrid");
            //console.log('result');
            //console.log(result);

            if (result.length == 0 || found) {
                found = true;
                //console.log('found place to insert');
                nonScheduleRow.insertBefore(nextRow);
            }

        }
    });

    // https://trello.com/c/C5e43WED/9-color-calendar-days-based-on-a-b-c

    $(".caldaycell .caldatacell").each(function( index ) {
        let elementDate = $(this).attr('id');

        //format is day_YYYYMMdd

        let year = elementDate.substring(4,8);
        let month = elementDate.substring(8,10);
        let day = elementDate.substring(10,12);
        let calClass = ShiftDate(new Date(year, month - 1, day));

        $(this).addClass("shift-class-" + calClass);
    });

    // Begin enhanced location view

    class Locations {
        constructor() {
            this.locations = new Array();
        }

        getAllLocations() {
            return this.locations;
        }

        addLocation(location) {
            this.locations.push(location);
        }
    }

    class Location {
        constructor(location) {
            this.name = location.name;
            this.battalion = location.battalion;
            this.station = location.station;
            this.apparatus = new Array();
        }

        getName() {
            return this.name;
        }

        addApparatus(apparatus) {
            this.apparatus.push(apparatus);
        }

        getApparatus() {
            return this.apparatus;
        }

        isValid() {

            let success = true;

            this.getApparatus().forEach( function(apparatus) {
                if(!(apparatus.isValid())) success = false;
            });

            return success;
        }
    }

    class Apparatus {
        constructor(name) {
            this.name = name;
            this.people = new Array();
            this.chiefCount = 0;
            this.officerCount = 0;
            this.driverCount = 0;
            this.paramedicCount = 0;
            this.peopleCount = 0;

            this.oneDay = 1000*60*60*24;
        }

        addPerson(person) {
            this.people.push(person);

            let scheduledArray = [
                '*Scheduled',
                '.Shipout',
                '.Hireback',
                '..Hireback',
                '.Acting as Driver',
                '.Acting as Lieutenant',
                'Acting as Battalion Chief',
                'Acting as Driver/Officer',
                '.Mandatory',
                '.Mandatory Elect',
                '.Swap In',
                '.Pro-Pay Off',
                'Acting as Paramedic',
                'Grant Hazmat ODP-Staffing Shortage',
                '.Meeting',
                '.SMART Swap',
                '.SMART - Mandatory',
                '.SMART Voluntary',
                'Act as Dr/Swap in',
                '.SMART Scheduled'
            ];

            if (!(scheduledArray.includes(person.status))) {

                //console.log('person is not working');
                //console.log(person);
                return;
            }


            let apparatus = this;

            switch (person.rank) {
                case 'PPO':
                    person.times.forEach( function(time) {
                        let dateStart = new Date(time.Start);
                        let dateEnd = new Date(time.Stop);

                        apparatus.paramedicCount += (dateEnd - dateStart);
                    });
                case 'CP':
                case 'LT':
                    person.times.forEach( function(time) {
                        let dateStart = new Date(time.Start);
                        let dateEnd = new Date(time.Stop);

                        apparatus.officerCount += (dateEnd - dateStart);
                        apparatus.peopleCount += (dateEnd - dateStart);

                        if (person.status == 'Acting as Driver/Officer') {
                            apparatus.driverCount += (dateEnd - dateStart);
                        }

                    });


                    break;

                case 'PM':

                    person.times.forEach( function(time) {
                        let dateStart = new Date(time.Start);
                        let dateEnd = new Date(time.Stop);

                        apparatus.paramedicCount += (dateEnd - dateStart);
                        apparatus.peopleCount += (dateEnd - dateStart);
                    });
                    break;

                case 'DR':

                    person.times.forEach( function(time) {
                        let dateStart = new Date(time.Start);
                        let dateEnd = new Date(time.Stop);

                        apparatus.driverCount += (dateEnd - dateStart);
                        apparatus.peopleCount += (dateEnd - dateStart);
                    });
                    break;

                case 'FF':
                    person.times.forEach( function(time) {
                        let dateStart = new Date(time.Start);
                        let dateEnd = new Date(time.Stop);

                        apparatus.peopleCount += (dateEnd - dateStart);
                    });
                    break;

                case 'BC':
                    person.times.forEach( function(time) {
                        let dateStart = new Date(time.Start);
                        let dateEnd = new Date(time.Stop);

                        apparatus.chiefCount += (dateEnd - dateStart);
                        apparatus.peopleCount += (dateEnd - dateStart);
                    });
                    break;
            }

        }

        getPersons() {
            return this.people;
        }

        validateLocation() { return {valid: true}; }

        validationString() { return ""; }
    }

    class Engine extends Apparatus {
        constructor(name) {
            super(name);
        }

        isValid() {
            return (
                ((this.peopleCount / this.oneDay) >= 4) &&
                ((this.officerCount / this.oneDay) >= 1) &&
                ((this.driverCount / this.oneDay) >= 1) &&
                ((this.paramedicCount / this.oneDay) >= 1)
            );
        }

        validationString() {

            let enoughPeople = ((this.peopleCount / this.oneDay) >= 4);
            let enoughOfficers = ((this.officerCount / this.oneDay) >= 1);
            let enoughDrivers = ((this.driverCount / this.oneDay) >= 1);
            let enoughMedics = ((this.paramedicCount / this.oneDay) >= 1);

            return `<span style="color: ${ enoughPeople ? 'green' : 'red' }">Personnel: ${(this.peopleCount / this.oneDay).toFixed(2)}</span>
                        <span style="color: ${ enoughOfficers ? 'green' : 'red' }">Officers: ${(this.officerCount / this.oneDay).toFixed(2)}</span>
                        <span style="color: ${ enoughDrivers ? 'green' : 'red' }">Drivers: ${(this.driverCount / this.oneDay).toFixed(2)}</span>
                        <span style="color: ${ enoughMedics ? 'green' : 'red' }">Medics: ${(this.paramedicCount / this.oneDay).toFixed(2)}</span>
`;

        }

    }

    class Truck extends Apparatus {
        constructor(name) {
            super(name);
        }

        isValid() {
            return (
                ((this.peopleCount / this.oneDay) >= 4) &&
                ((this.officerCount / this.oneDay) >= 1) &&
                ((this.driverCount / this.oneDay) >= 1)
            );
        }

        validationString() {

                let enoughPeople = ((this.peopleCount / this.oneDay) >= 4);
                let enoughOfficers = ((this.officerCount / this.oneDay) >= 1);
                let enoughDrivers = ((this.driverCount / this.oneDay) >= 1);

                return `<span style="color: ${ enoughPeople ? 'green' : 'red' }">Personnel: ${(this.peopleCount / this.oneDay).toFixed(2)}</span>
                        <span style="color: ${ enoughOfficers ? 'green' : 'red' }">Officers: ${(this.officerCount / this.oneDay).toFixed(2)}</span>
                        <span style="color: ${ enoughDrivers ? 'green' : 'red' }">Drivers: ${(this.driverCount / this.oneDay).toFixed(2)}</span>
`;

        }

    }

    class Rescue extends Apparatus {
        constructor(name) {
            super(name);
        }

        isValid() {
            return (
                ((this.peopleCount / this.oneDay) >= 4) &&
                ((this.officerCount / this.oneDay) >= 1) &&
                ((this.driverCount / this.oneDay) >= 1)
            );
        }

        validationString() {

                let enoughPeople = ((this.peopleCount / this.oneDay) >= 4);
                let enoughOfficers = ((this.officerCount / this.oneDay) >= 1);
                let enoughDrivers = ((this.driverCount / this.oneDay) >= 1);

                return `<span style="color: ${ enoughPeople ? 'green' : 'red' }">Personnel: ${(this.peopleCount / this.oneDay).toFixed(2)}</span>
                        <span style="color: ${ enoughOfficers ? 'green' : 'red' }">Officers: ${(this.officerCount / this.oneDay).toFixed(2)}</span>
                        <span style="color: ${ enoughDrivers ? 'green' : 'red' }">Drivers: ${(this.driverCount / this.oneDay).toFixed(2)}</span>
`;

        }
    }

    class Hazmat extends Apparatus {
        constructor(name) {
            super(name);
        }

        isValid() {
            return (
                ((this.peopleCount / this.oneDay) >= 4) &&
                ((this.officerCount / this.oneDay) >= 1) &&
                ((this.driverCount / this.oneDay) >= 1)
            );
        }

        validationString() {

                let enoughPeople = ((this.peopleCount / this.oneDay) >= 4);
                let enoughOfficers = ((this.officerCount / this.oneDay) >= 1);
                let enoughDrivers = ((this.driverCount / this.oneDay) >= 1);

                return `<span style="color: ${ enoughPeople ? 'green' : 'red' }">Personnel: ${(this.peopleCount / this.oneDay).toFixed(2)}</span>
                        <span style="color: ${ enoughOfficers ? 'green' : 'red' }">Officers: ${(this.officerCount / this.oneDay).toFixed(2)}</span>
                        <span style="color: ${ enoughDrivers ? 'green' : 'red' }">Drivers: ${(this.driverCount / this.oneDay).toFixed(2)}</span>
`;

        }
    }

    class Chief extends Apparatus {
        constructor(name) {
            super(name);
        }

        isValid() {
            return (
                ((this.peopleCount / this.oneDay) >= 1) &&
                ((this.chiefCount / this.oneDay) >= 1)
            );
        }

        validationString() {

                let enoughChiefs = ((this.chiefCount / this.oneDay) >= 1);

                return `<span style="color: ${ enoughChiefs ? 'green' : 'red' }">Chiefs: ${(this.chiefCount / this.oneDay).toFixed(2)}</span>`;

        }
    }

    class Squad extends Apparatus {
        constructor(name) {
            super(name);
        }

        isValid() {
            if (isNaN(this.peopleCount / this.OneDay)) {
                return true;
            } else {
                return (
                    ((this.peopleCount / this.oneDay) >= 2) &&
                    ((this.paramedicCount / this.oneDay) >= 1)
                );
            }
        }

        validationString() {

                let enoughPeople = ((this.peopleCount / this.oneDay) >= 2);
                let enoughMedics = ((this.paramedicCount / this.oneDay) >= 1);

                return `<span style="color: ${ enoughPeople ? 'green' : 'red' }">Personnel: ${(this.peopleCount / this.oneDay).toFixed(2)}</span>
                        <span style="color: ${ enoughMedics ? 'green' : 'red' }">Medics: ${(this.paramedicCount / this.oneDay).toFixed(2)}</span>
`;

        }
    }

    class Other extends Apparatus {
        constructor(name) {
            super(name);
        }

    }

    let Engine1 = new Engine('Engine 1');
    let Engine2 = new Engine('Engine 2');
    let Engine3 = new Engine('Engine 3');
    let Engine4 = new Engine('Engine 4');
    let Engine5 = new Engine('Engine 5');
    let Engine6 = new Engine('Engine 6');
    let Engine7 = new Engine('Engine 7');
    let Engine8 = new Engine('Engine 8');
    let Engine9 = new Engine('Engine 9');
    let Engine10 = new Engine('Engine 10');
    let Engine11 = new Engine('Engine 11');
    let Engine12 = new Engine('Engine 12');
    let Engine13 = new Engine('Engine 13');
    let Engine14 = new Engine('Engine 14');
    let Engine15 = new Engine('Engine 15');
    let Engine16 = new Engine('Engine 16');
    let Engine17 = new Engine('Engine 17');
    let Engine18 = new Engine('Engine 18');
    let Engine19 = new Engine('Engine 19');
    let Engine20 = new Engine('Engine 20');
    let Engine21 = new Engine('Engine 21');
    let Engine22 = new Engine('Engine 22');
    let Engine23 = new Engine('Engine 23');

    let Truck1 = new Truck('Truck 1');
    let Truck4 = new Truck('Truck 4');
    let Truck8 = new Truck('Truck 8');
    let Truck9 = new Truck('Truck 9');
    let Truck10 = new Truck('Truck 10');
    let Truck19 = new Truck('Truck 19');

    let Squad7 = new Squad('Squad 7');
    let Squad1 = new Squad('Squad 1');
    let Squad11 = new Squad('Squad 11');

    let Chief1 = new Chief('BC 1');
    let Chief2 = new Chief('BC 2');
    let Chief3 = new Chief('BC 3');
    let Chief4 = new Chief('BC 4');

    let Rescue17 = new Rescue('Rescue 17');

    let Hazmat14 = new Hazmat('Hazmat 14');


    let Station1 = new Location({ name: 'ST 1', battalion: 1, station: 1});
    let Station2 = new Location({ name: 'ST 2', battalion: 2, station: 2});
    let Station3 = new Location({ name: 'ST 3', battalion: 1, station: 3});
    let Station4 = new Location({ name: 'ST 4', battalion: 1, station: 4});
    let Station5 = new Location({ name: 'ST 5', battalion: 1, station: 5});
    let Station6 = new Location({ name: 'ST 6', battalion: 2, station: 6});
    let Station7 = new Location({ name: 'ST 7', battalion: 2, station: 7});
    let Station8 = new Location({ name: 'ST 8', battalion: 2, station: 8});
    let Station9 = new Location({ name: 'ST 9', battalion: 3, station: 9});
    let Station10 = new Location({ name: 'ST 10', battalion: 2, station: 10});
    let Station11 = new Location({ name: 'ST 11', battalion: 2, station: 11});
    let Station12 = new Location({ name: 'ST 12', battalion: 3, station: 12});
    let Station13 = new Location({ name: 'ST 13', battalion: 1, station: 13});
    let Station14 = new Location({ name: 'ST 14', battalion: 3, station: 14});
    let Station15 = new Location({ name: 'ST 15', battalion: 4, station: 15});
    let Station16 = new Location({ name: 'ST 16', battalion: 1, station: 16});
    let Station17 = new Location({ name: 'ST 17', battalion: 4, station: 17});
    let Station18 = new Location({ name: 'ST 18', battalion: 3, station: 18});
    let Station19 = new Location({ name: 'ST 19', battalion: 3, station: 19});
    let Station20 = new Location({ name: 'ST 20', battalion: 4, station: 20});
    let Station21 = new Location({ name: 'ST 21', battalion: 4, station: 21});
    let Station22 = new Location({ name: 'ST 22', battalion: 3, station: 22});
    let Station23 = new Location({ name: 'ST 23', battalion: 2, station: 23});

    Station1.addApparatus(Chief1);
    Station1.addApparatus(Engine1);
    Station1.addApparatus(Truck1);
    Station1.addApparatus(Squad1);

    Station2.addApparatus(Engine2);

    Station3.addApparatus(Engine3);

    Station4.addApparatus(Engine4);
    Station4.addApparatus(Truck4);

    Station5.addApparatus(Engine5);

    Station6.addApparatus(Engine6);

    Station7.addApparatus(Engine7);
    Station7.addApparatus(Squad7);

    Station8.addApparatus(Chief2);
    Station8.addApparatus(Engine8);
    Station8.addApparatus(Truck8);

    Station9.addApparatus(Engine9);
    Station9.addApparatus(Truck9);

    Station10.addApparatus(Engine10);
    Station10.addApparatus(Truck10);

    Station11.addApparatus(Engine11);
    Station11.addApparatus(Squad11);

    Station12.addApparatus(Engine12);

    Station13.addApparatus(Engine13);

    Station14.addApparatus(Engine14);
    Station14.addApparatus(Hazmat14);

    Station15.addApparatus(Engine15);

    Station16.addApparatus(Engine16);

    Station17.addApparatus(Engine17);
    Station17.addApparatus(Rescue17);

    Station18.addApparatus(Engine18);

    Station19.addApparatus(Chief3);
    Station19.addApparatus(Engine19);
    Station19.addApparatus(Truck19);

    Station20.addApparatus(Chief4);
    Station20.addApparatus(Engine20);

    Station21.addApparatus(Engine21);

    Station22.addApparatus(Engine22);

    Station23.addApparatus(Engine23);

    locations = new Locations();

    locations.addLocation(Station1);
    locations.addLocation(Station2);
    locations.addLocation(Station3);
    locations.addLocation(Station4);
    locations.addLocation(Station5);
    locations.addLocation(Station6);
    locations.addLocation(Station7);
    locations.addLocation(Station8);
    locations.addLocation(Station9);
    locations.addLocation(Station10);
    locations.addLocation(Station11);
    locations.addLocation(Station12);
    locations.addLocation(Station13);
    locations.addLocation(Station14);
    locations.addLocation(Station15);
    locations.addLocation(Station16);
    locations.addLocation(Station17);
    locations.addLocation(Station18);
    locations.addLocation(Station19);
    locations.addLocation(Station20);
    locations.addLocation(Station21);
    locations.addLocation(Station22);
    locations.addLocation(Station23);

    var LocationArray = new Array();

    LocationArray[209] = Engine1;
    LocationArray[227] = Engine2;
    LocationArray[212] = Engine3;
    LocationArray[214] = Engine4;
    LocationArray[217] = Engine5;
    LocationArray[231] = Engine6;
    LocationArray[235] = Engine7;
    LocationArray[240] = Engine8;
    LocationArray[224] = Engine9;
    LocationArray[248] = Engine10;
    LocationArray[252] = Engine11;
    LocationArray[218] = Engine12;
    LocationArray[221] = Engine13;
    LocationArray[230] = Engine14;
    LocationArray[257] = Engine15;
    LocationArray[223] = Engine16;
    LocationArray[260] = Engine17;
    LocationArray[239] = Engine18;
    LocationArray[244] = Engine19;
    LocationArray[263] = Engine20;
    LocationArray[266] = Engine21;
    LocationArray[253] = Engine22;
    LocationArray[273] = Engine23;

    LocationArray[210] = Truck1;
    LocationArray[215] = Truck4;
    LocationArray[242] = Truck8;
    LocationArray[216] = Truck9;
    LocationArray[249] = Truck10;
    LocationArray[245] = Truck19;

    LocationArray[238] = Squad7;
    LocationArray[243] = Squad1;
    LocationArray[254] = Squad11;

    LocationArray[211] = Chief1;
    LocationArray[246] = Chief2;
    LocationArray[250] = Chief3;
    LocationArray[265] = Chief4;

    LocationArray[262] = Rescue17;

    LocationArray[236] = Hazmat14;

    class Person {
        constructor(person) {
            this.name = person.name;
            this.rank = person.rank;
            this.skills = person.skills;
            this.times = person.times;
            this.comments = person.comments;
            this.status = person.status;
            this.startTime = person.startTime;
            this.endTime = person.endTime;
        }
    }

    if (typeof window.DayInitialDate !== "undefined") {
        var dayOf = window.DayInitialDate;
        dayOf.setHours(8, 0, 0, 0);
        var dayEnd = new Date(dayOf);
        dayEnd.setDate(dayOf.getDate() + 1);
    }

    if (typeof window.SchedObs != "undefined") {
        $.each(window.SchedObs, function(index, value) {
            if ((value.UserID > 0) && (value.Times.length > 0)) {


                let startTime = value.Times[0].Start;
                let endTime = 0;


                value.Times.forEach( function(time) {
                    endTime = time.Stop;
                });

                let name = '';
                let skills = '';

                try {
                    name = window.arrUserInformations[value.UserID].EmpName;
                } catch (e) {
                    console.log(e);
                    name = 'Employee Missing Shift Schedule';
                }

                try {
                    skills = window.arrUserInformations[value.UserID].SkillString;
                } catch (e) {
                    console.log(e);
                    skills = 'Employee Missing Shift Schedule';
                }

                let tempPerson = {
                    name: name,
                    rank: window.arrJobs[value.JobID],
                    skills: skills,
                    status: window.arrShiftStatuses[value.StatusID].Name,
                    times: value.Times,
                    comments: value.Comments,
                    startTime: startTime,
                    endTime: endTime
                };

                let person = new Person(tempPerson);

                try {
                    LocationArray[value.LocID].addPerson(person);
                } catch (e) {
                    console.log('could not add person to location ' + value.LocID);
                    console.log(person);
                    console.log(e);
                }
                //console.log(value.LocID);


                //console.log(window.arrUserInformations[value.UserID].EmpName);
                //console.log(window.arrJobs[value.JobID]);
                //console.log(window.arrUserInformations[value.UserID].SkillString);
                //console.log(window.arrShiftStatuses[value.StatusID].Name);
                //console.log(value.Times);
                //console.log(value.Comments);
                //console.log(window.arrLocs[value.LocID]);
            }
            //console.log(value);


        });

        console.log(locations.getAllLocations());

        var enhancedLocationView = `
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Enhanced Staffing View</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <style>
            .job-LT, .job-CP { background: red; color: white; }
            .job-PPO { background: linear-gradient(to right, blue 16.6%, red 16.6%); color: white;}
            .job-DR { background: black; color: white; }
            .job-PM { background: blue; color: white; }
            .job-FF { background: yellow; color: black; }
            .job-FF.job-FF-4-A { background: pink; color: black; }
            .job-FF.job-FF-4-B { background: rgb(209, 118, 53); color: white; }
            .person.row { box-shadow: 0px -1px 0px black inset; }
            .card-header span { margin-left: 20px; }
            .person:hover { opacity: .6; }
        </style>

    </head>
    <body>
        <div class="container">
            <div class="card">
                <div class="card-header">Shift Date: ${window.DayInitialDate.toDateString()}</div>
                    <div class="card-body">
                        <div class="row text-center" style="margin-bottom: 20px;"><ul class="nav nav-tabs">
`;

locations.getAllLocations().forEach( function(location) {
    enhancedLocationView += `<li class="col-1 mb-3"><a href="#station-${location.station}" data-toggle="tab" type="button" class="btn btn-${location.isValid() ? "success" : "danger"}">${location.station}</a></li>`;
});

            enhancedLocationView += `</ul></div>
                    </div>
                </div>
            </div> <!-- closes card -->
            <div class="container-fluid tab-content" id="locations" style="font-size: 14px;">`;

        locations.getAllLocations().forEach( function(location) {
            enhancedLocationView += `<div class="tab-pane fade in" id="station-${location.station}"><div class="card location" data-station="${location.station}" data-battalion="${location.battalion}" >
                                         <div class="card-header pt-1 pb-1">${location.name} - Battalion ${location.battalion}</div>`;
            enhancedLocationView += `    <div class="card-body pt-1 pb-1">`;
            location.getApparatus().forEach( function(apparatus) {
                enhancedLocationView += `    <div class="card">
                                                 <div class="card-header pt-1 pb-1">${apparatus.name} ${apparatus.validationString()}</div>
                                                 <div class="card-body pt-1 pb-1">`;

                apparatus.getPersons().forEach( function(person) {
                    enhancedLocationView += `
                                                     <div class="person row job-${person.rank} ${ person.skills.includes('4C') ? 'job-FF-4-A' : '' }">
                                                         <div class="col-2" style="white-space: nowrap; overflow: hidden;" ><span class="person-name" style="text-overflow: ellipsis; ">${person.name}</span></div>
                                                         <div class="col-1"><span class="person-job">${person.rank}</span></div>
                                                         <div class="col-2"><span class="person-skills">${$(person.skills).text()}</span></div>
                                                         <div class="col-2"><span class="person-shift-status">${person.status}</span></div>
                                                         <div class="col-2" style="box-shadow: 0px -1px 0px black inset; color: white; background: linear-gradient(to right,
                                                             grey 0% ${(((new Date(person.startTime) - new Date(dayOf)) / (24*60*60*1000))*100)}%,
                                                             green ${(((new Date(person.startTime) - new Date(dayOf)) / (24*60*60*1000))*100)}%
                                                             ${((((new Date(dayEnd) - new Date(person.endTime) - (24*60*60*1000))) *-1) / (24*60*60*1000))*100}%,
                                                             grey ${((((new Date(dayEnd) - new Date(person.endTime) - (24*60*60*1000))) *-1) / (24*60*60*1000))*100}% 100%
                                                             )">
                                                             <span class="person-start-time">${String(new Date(person.startTime).getHours()).padStart(2, "0") + ":" + String(new Date(person.startTime).getMinutes()).padStart(2, "0")}</span>
                                                             <span class="person-end-time">${String(new Date(person.endTime).getHours()).padStart(2, "0") + ":" + String(new Date(person.endTime).getMinutes()).padStart(2, "0")}</span>
                                                         </div>
                                                         <div class="col-3"><span class="person-comments">${person.comments}</span></div>
                                                     </div>`;
                });

                enhancedLocationView += `
                                                 </div>
                                             </div>`;
            });

            enhancedLocationView += `    </div></div></div>`;
        });


        enhancedLocationView += `
        </div>

        <script type="text/javascript">
            var nonScheduled = $(".person .person-shift-status:not(" +
        ":contains('Scheduled')," +
        ":contains('Shipout')," +
        ":contains('Hireback')," +
        ":contains('Acting as Driver')," +
        ":contains('Acting as Lieutenant')," +
        ":contains('Acting as Battalion Chief')," +
        ":contains('Acting as Driver/Officer')," +
        ":contains('Swap In')," +
        ":contains('Pro-Pay Off')," +
        ":contains('Mandatory')," +
        ":contains('Acting as Paramedic')," +
        ":contains('Grant Hazmat ODP-Staffing Shortage')," +
        ":contains('.SMART Voluntary')," +
        ":contains('.SMART Swap')," +
        ":contains('.SMART - Mandatory')," +
        ":contains('Act as Dr/Swap in')," +
        ":contains('Meeting')" +
            ")")

            nonScheduled.each( function(index) {
                let nonScheduleRow = $(this).parent().parent();
                nonScheduleRow.appendTo(nonScheduleRow.parent());
                nonScheduleRow.css({"background": "grey", "color": "white"});
            });

        </script>

        <script type="text/javascript">
            $('#battalion-sort').click( function() {
                var divList = $(".card.location");
                divList.sort(function(a, b){
                    return $(a).data("battalion")-$(b).data("battalion")
                });

                $("#locations").html(divList);
            });

            $('#station-sort').click( function() {
                var divList = $(".card.location");
                divList.sort(function(a, b){
                    return $(a).data("station")-$(b).data("station")
                });

                $("#locations").html(divList);
            });

        </script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    </body>
</html>
`;

        window.enhancedLocationView = enhancedLocationView;
        $('head').append('<script>function openEnhancedView() { var enhancedWindow = window.open("", "enhancedLocationWindow", "width=1200,height=800"); enhancedWindow.document.write(window.enhancedLocationView); enhancedWindow.document.close() }</script>');
        $('body').prepend('<button onclick="openEnhancedView()">Enhanced View</button>');

        //$('body').prepend('<iframe style="margin-bottom: 20px;" id="enhanced-location-view" src="about:blank" width="1300px" height="700px" frameBorder="0" />');
        //$('#enhanced-location-view').attr("srcdoc", enhancedLocationView);
    }

}); })(window.jQuery.noConflict(true));